<?php
namespace App\Repositories\Blog;

use App\Repositories\BaseRepository;

class BlogRepository extends BaseRepository implements BlogRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Blog::class;
    }

    public function getBlog()
    {
        return $this->model->select('product_name')->take(5)->get();
    }
}