<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'title'=>'required|min:5',
                    'des'=>'required',
                    'detail'=>'required',
                    'category'=>'required',
                    'data_pubblic'=>'required',
                    'position'=>'required'
                ];
                break;

            case 'PUT':
                return [
                    'title'=>'required|min:5',
                    'des'=>'required',
                    'detail'=>'required',
                    'category'=>'required|numeric',
                    'data_pubblic'=>'required',
                    'position'=>'required'
                ];
                break;
            
            default:
                # code...
                break;
            }
    }

    public function messages(){
        return [
            'title.required' => 'Đừng có để trống title làm ơn',
            'title.min' => 'Nhập lớn hơn 5 chữ đi, lười thế',
            'des.required' => 'Đừng có để trống des làm ơn',
            'detail.required' => 'Đừng có để trống detail làm ơn',
            'category.required' => 'Đừng có để trống detail làm ơn',
            'category.numeric' => 'Số, Hãy nhập số, Làm ơn',
            'data_pubblic.required' => 'Đừng có để trống detail làm ơn',
            'position.required' => 'Đừng có để trống detail làm ơn'
        ];
    }
}
