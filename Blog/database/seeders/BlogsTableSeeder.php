<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('blogs')->insert([
            [
                'title'=>'ABC',
                'des'=>'XYZPRO',
                'detail'=>'XDXDXDX',
                'category'=>1,
                'data_pubblic'=>'2021/07/14',
                'public'=>true,
                'position'=>'1,2',
                'thumbs'=>'abc.jpg'
            ],
            [
                'title'=>'ABC1111111111',
                'des'=>'XYZPR11111111111O',
                'detail'=>'XDXDXD11111111111111X',
                'category'=>2,
                'data_pubblic'=>'2021/07/14',
                'public'=>true,
                'position'=>'1,2',
                'thumbs'=>'abc.jpg'
            ]
        ]);
    }
}
